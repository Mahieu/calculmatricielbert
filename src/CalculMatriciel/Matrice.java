package CalculMatriciel;
import java.util.*;

public class Matrice {
	private int nbligne, nbcol;
	private ArrayList<Ligne> lignes;
	Matrice(int l,int c){
		this.nbligne=l;
		this.nbcol=c;
		lignes = new ArrayList<Ligne>();
	}
	public int getNbcol() {
		return nbcol;
	}
	public void setNbcol(int nbcol) {
		this.nbcol = nbcol;
	}
	public int getNbligne() {
		return nbligne;
	}
	public void setNbligne(int nbligne) {
		this.nbligne = nbligne;
	}
	public void remplacerLigne(int index, Ligne ligne)
	{
		this.lignes.set(index, ligne);
	}
	public void ajouterLigne(Ligne l){
		/*if(lignes.add(l))
			setNbligne(lignes.size());*/
		this.lignes.add(l);
	}
	public Ligne getLigne(int index ){
		return lignes.get(index);
	}
	public String toString(){
			String tmp= "Tableau :\n";
			for(Ligne ligne : lignes)
			{
				tmp+=ligne.toString();
			}
			return tmp;
	}
	public Matrice clone(){
		Matrice a = new Matrice(this.getNbligne(), this.getNbcol());
		for(int i=0;i<this.getNbligne();i++){
			a.ajouterLigne(this.getLigne(i));
		}
		return a;
	}
	
}
