package CalculMatriciel;

public class GaussJordan{

    public Matrice matriceAugmentee;
	public Matrice matriceIdentitee; 
    /**
     * Constructor for a GaussJordan object. Takes in a two dimensional double
     * array holding the matrix.
     *
     * @param matrix A double[][] containing the augmented matrix
     */
    public GaussJordan(Matrice matrice) {
    	matriceAugmentee = matrice;
    	matriceIdentitee = creationIdentite(matrice);
    }

    /**
     * Runs a Gauss-Jordan elimination on the augmented matrix in order to put
     * it into reduced row echelon form
     *
     */
    public void eliminate() {
        int startColumn = 0;
        for (int currentLine=0; currentLine<matriceAugmentee.getNbligne(); currentLine++) {
            //if the number in the start column is 0, try to switch with another
            while (matriceAugmentee.getLigne(currentLine).get(startColumn)==0.0){
                boolean switched = false;
                int i=currentLine;
                while (!switched && i<matriceAugmentee.getNbligne()) {
                    if(matriceAugmentee.getLigne(i).get(startColumn)!=0.0){
                        Ligne temp = matriceAugmentee.getLigne(i);
                        matriceAugmentee.remplacerLigne(i, matriceAugmentee.getLigne(currentLine));
                        matriceAugmentee.remplacerLigne(currentLine,temp);
                        switched = true;
                    }
                    i++;
                }
                //if after trying to switch, it is still 0, increase column
                if (matriceAugmentee.getLigne(currentLine).get(startColumn)==0.0) {
                    startColumn++;
                }
            }
            //if the number isn't one, reduce to one c est ici que �a se passe pour la matrice identit�e 
            if(matriceAugmentee.getLigne(currentLine).get(startColumn)!=1.0) {
                double divisor = matriceAugmentee.getLigne(currentLine).get(startColumn);
                for (int i=startColumn; i<matriceAugmentee.getNbcol(); i++) {
                	matriceAugmentee.getLigne(currentLine).modifierCelulle(i, matriceAugmentee.getLigne(currentLine).get(i)/divisor);
                }
                for (int i=0; i<matriceAugmentee.getNbcol() - 1; i++) {
                	matriceIdentitee.getLigne(currentLine).modifierCelulle(i, matriceIdentitee.getLigne(currentLine).get(i)/divisor);
                }
            }
            //make sure the number in the start column of all other rows is 0
            for (int i=0; i<matriceAugmentee.getNbligne(); i++) {
                if (i!=currentLine && matriceAugmentee.getLigne(i).get(startColumn)!=0) {
                    double multiple = 0-matriceAugmentee.getLigne(i).get(startColumn);
                    for (int j=startColumn; j<matriceAugmentee.getNbcol(); j++){
                    	double valeur  = matriceAugmentee.getLigne(i).get(j) + (multiple*matriceAugmentee.getLigne(currentLine).get(j));
                    	matriceAugmentee.getLigne(i).modifierCelulle(j, valeur);
                    }
                    for (int colId=0; colId<matriceAugmentee.getNbcol() -1; colId++){
                    	double valeurId  = matriceIdentitee.getLigne(i).get(colId) + (multiple*matriceIdentitee.getLigne(currentLine).get(colId));
                    	matriceIdentitee.getLigne(i).modifierCelulle(colId, valeurId);
                    }
                }
            }
            startColumn++;
        }
    }
    private static Matrice creationIdentite(Matrice m){
		Matrice matriceId = new Matrice(m.getNbligne(), m.getNbcol()-1);
		double ligneTmp[] = new double[m.getNbcol()-1];
		for(int i=0; i<m.getNbligne();i++){
			for(int j=0;j<m.getNbcol()-1;j++){
				if(i==j){
					ligneTmp[j] = 1;
				}else{
					ligneTmp[j] = 0;
				}
			}
			matriceId.ajouterLigne(new Ligne(m.getNbcol()-1,ligneTmp));
		}
		return matriceId;
	}
}