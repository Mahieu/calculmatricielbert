package CalculMatriciel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
public class Ligne {
	private int nbVar ;// nombre de variable que poss�dent les contraintes et nombre de variables de d�cisions	
	private List<Double> cellules ;// liste des Cellules chaque cellule est un double
	public Ligne(int nbVar,double var[]) {
		super();
		this.nbVar = nbVar;
		this.cellules = new ArrayList<Double>() ;
		//cette boucle permet d'ajouter les variable entr�e par l'utilisateur dans la liste de cellules
		for (int i =0 ; i<nbVar ; i++){
			this.cellules.add(var[i]);
		}
	}
	
	public int getNbVar() {
		return this.nbVar;
	}
	public void setNbVar(int nbVar) {
		this.nbVar = nbVar;
	}
	/**
	 * Cette m�thode permet d'ajouter une cellule � la ligne
	 * @param c Valeur � ajouter
	 */
	public void ajouterCellule(double c){
		 this.cellules.add(c) ;
	}
	/**
	 * Affichage
	 */
	public String toString()
	{
		NumberFormat nf = new DecimalFormat("0.##");
		String tmp = "";
		for(double chiffre : cellules)
		{
			tmp+=nf.format(chiffre)+"\t";
		}
		tmp+="\n";
		return tmp;
	}
	/**
	 * Permet de modifier la valeur d'une cellule du tableau
	 * @param index	Index de la cellule � modifier
	 * @param c		Valeur � affecter
	 */
	public void modifierCelulle(int index, double c) {
		this.cellules.set(index, c);
	}
	public Double get(int index)
	{
		return this.cellules.get(index);
	}

}
