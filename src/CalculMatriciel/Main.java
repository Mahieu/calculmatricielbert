package CalculMatriciel;
import java.util.*;
public class Main {

	public static void main(String[] args) {
		// TODO Determinant, inversion de matrice, Reduction(pour trouver determinant et matrice inverse) puis gauss jordan
		//TODO DETERMINANT A REFAIRE, MATRICE DE COFFACTEUR OK 
		//TODO DETERMINANT est maintenant ok , MATRICE DE COFFACTEUR OK 
		/*Tableau a = new Tableau(2,3);
		Tableau b = new Tableau(3,2);
		a.ajouterLigne(new Ligne();*/
		ArrayList<Matrice> matrices=new ArrayList<Matrice>();
		Scanner lc = new Scanner(System.in);
		System.out.println("combien de matrice");
		int x=lc.nextInt();
			for(int a=0;a<x;a++){
				System.out.println("Entrez le nombre de lignes de la matrice "+a+"\n");
				int li=lc.nextInt();
				System.out.println("Entre le nombre de colonnes de la matrice "+a+"\n");
				int co=lc.nextInt();
				matrices.add(new Matrice(li,co));
				double[] tmp=new double[co];
				for(int i=0;i<li;i++){
					System.out.println("Entrez ligne "+(i+1)+" de la matrice "+a+"\n");
					for(int j=0;j<co;j++){
						
						tmp[j]=lc.nextDouble();
					}
					matrices.get(a).ajouterLigne(new Ligne(co, tmp));
				}
			}
			Matrice res;
			res = matrices.get(0).clone();
			/*System.out.println("Entrez :\n1 => addition\n2 => soustraction\n3 => produit scalaire\n4 => produit matriciel");
			int choix=lc.nextInt();
			switch (choix){
			case 1:
				res=new Matrice(matrices.get(0).getNbligne(),matrices.get(0).getNbcol());
				res=matrices.get(0).clone();
				for(int i=1;i<matrices.size();i++){
					res=addition(res, matrices.get(i));
				}
				break;
			case 2:
				res=new Matrice(matrices.get(0).getNbligne(),matrices.get(0).getNbcol());
				res=matrices.get(0).clone();
				for(int i=1;i<matrices.size();i++){
					res=soustraction(res, matrices.get(i));
				}
				break;
			case 3:
				res=new Matrice(matrices.get(0).getNbligne(),matrices.get(0).getNbcol());
				System.out.println("entrez le produit :");
				double produit=lc.nextDouble();
				res=produitScalaire(produit, matrices.get(0));
				break;
			case 4:
				res=matrices.get(0).clone();
				for(int i=1;i<matrices.size();i++){
					res=multiplication(res, matrices.get(i));
				}
				break;
			default:
					res=new Matrice(1, 1);
			}
			System.out.println(res.toString());
		*/
		GaussJordan gaussipGirls = new GaussJordan(res);
		gaussipGirls.eliminate();
		System.out.println(gaussipGirls.matriceAugmentee.toString());
		System.out.println(gaussipGirls.matriceIdentitee.toString());
		//Matrice res=new Matrice(li,co);
		//res.ajouterLigne(new Ligne(a.getLigne(i)));
		//System.out.println(a.toString()+"\n + \n"+b.toString()+"\n = \n"+addition(a,b).toString());
		//System.out.println("\n\n\n");
		//System.out.println(addition(a,b).toString()+"\n * \n 2 \n = \n"+produitScalaire(2, addition(a,b)).toString());
		//System.out.println(a.toString()+"\n x \n"+b.toString()+"\n = \n"+multiplication(a, b).toString());

	}
	
	public static Matrice addition(Matrice a, Matrice b) {
		if(a.getNbligne()==b.getNbligne() && a.getNbcol()==b.getNbcol()){
			//System.out.println(b.getNbligne()+" "+ b.getNbcol());
			Matrice res=new Matrice(a.getNbligne(), a.getNbcol());
			double[] cell=new double[a.getNbcol()];
			for(int i=0; i<a.getNbligne();i++){
				//System.out.println(i+"<"+a.getNbligne());
				int j=0;
				for(j=0;j<a.getNbcol();j++){
					//System.out.println(i+","+j);
					//System.out.println("ligne , cellule : "+a.getLigne(i).get(i));
					cell[j]=a.getLigne(i).get(j)+b.getLigne(i).get(j);
					//System.out.println(cell[j]);
				}
				res.ajouterLigne(new Ligne(j,cell));
			}
			return res;
		}
		return null;
	}
	public static Matrice soustraction(Matrice a, Matrice b) {
		if(a.getNbligne()==b.getNbligne() && a.getNbcol()==b.getNbcol()){
			//System.out.println(b.getNbligne()+" "+ b.getNbcol());
			Matrice res=new Matrice(a.getNbligne(), a.getNbcol());
			double[] cell=new double[a.getNbcol()];
			for(int i=0; i<a.getNbligne();i++){
				//System.out.println(i+"<"+a.getNbligne());
				int j=0;
				for(j=0;j<a.getNbcol();j++){
					//System.out.println(i+","+j);
					//System.out.println("ligne , cellule : "+a.getLigne(i).get(i));
					cell[j]=a.getLigne(i).get(j) - b.getLigne(i).get(j);
					//System.out.println(cell[j]);
				}
				res.ajouterLigne(new Ligne(j,cell));
			}
			return res;
		}
		return null;
	}
	public static Matrice produitScalaire(double x, Matrice a){
		Matrice res=new Matrice(a.getNbligne(),a.getNbcol());
		double[] cell=new double[a.getNbcol()];
		for(int i=0;i<a.getNbligne();i++){
			for(int j=0;j<a.getNbcol();j++){
				cell[j]=x*a.getLigne(i).get(j);
			}
			res.ajouterLigne(new Ligne(a.getNbcol(),cell));
		}
		return res;
	}
	public static Matrice multiplication(Matrice a, Matrice b){
		if(a.getNbcol()==b.getNbligne()){
			Matrice res=new Matrice(a.getNbligne(),b.getNbcol());
			double[] tmp= new double[b.getNbcol()];
			if(a.getNbcol()==b.getNbligne()){
				for(int i=0;i<a.getNbligne();i++){
					for(int j=0;j<b.getNbcol();j++){
						tmp[j]=0;
						for(int k=0;k<a.getNbligne();k++){
							tmp[j]=tmp[j]+a.getLigne(i).get(k)*b.getLigne(k).get(j);
						}
					}
					res.ajouterLigne(new Ligne(b.getNbcol(),tmp));
				}
			}
			return res;
		}
		return null;
	}
	/**
	 * Cette m�thode permet de supprimer une ligne et une colonne de fa�on humaine
	 * @param li Le num�ro de la ligne � supprimer (en humain)
	 * @param col Le num�ro de la ligne � supprimer (en humain)
	 * @param m La matrice sur laquelle on doit retirer la ligne et la colonne
	 * @return Une matrice sans la colonne et la ligne
	 */
	public static Matrice reduction(int li, int col, Matrice m){
		li--;
		col--;
		Matrice a= new Matrice(m.getNbligne()-1,m.getNbcol()-1);
		int numberOfColumnsM = m.getNbcol();
		int numberOfRowsM = m.getNbligne();
		int currentColumnOfLineToAdd = 0;
		double[] lineToAdd = new double[numberOfColumnsM-1];
		Ligne tmpLine;
		for(int currentRow = 0;currentRow<numberOfRowsM;currentRow++)
		{
			
			currentColumnOfLineToAdd = 0;
			if(li == currentRow)
			{
				continue;
			}
			for(int currentCol = 0; currentCol<numberOfColumnsM;currentCol++)
			{
				
				if(currentCol == col)
				{
					continue;
				}
				lineToAdd[currentColumnOfLineToAdd] = m.getLigne(currentRow).get(currentCol);
				currentColumnOfLineToAdd++;
				
			}
			tmpLine = new Ligne(numberOfColumnsM-1, lineToAdd);
			a.ajouterLigne(tmpLine);
			
		}
		return a;
	}
	public static double determinant(Matrice m){
		
		double det = 0;
		if(m.getNbcol()==1){
			det=m.getLigne(0).get(0);
		}else{
			for(int i=0;i<m.getNbligne();i++){
				Matrice m2= reduction(1, i+1, m);
				det+=Math.pow(-1, i)*m.getLigne(0).get(i)*determinant(m2);
			}
		}
		
		return det;
	}
	public static Matrice coffacteur(Matrice m){
		Matrice a=new Matrice(m.getNbligne(), m.getNbcol());
		double[] ligne=new double[m.getNbcol()];
         for (int i = 0; i<m.getNbligne(); i++)
         {
             for (int j = 0; j<m.getNbcol(); j++)
             {
            	 Matrice tmpMatriceReduite = reduction(i+1, j+1, m);
            	 double tmpDeter = determinant(tmpMatriceReduite);
            	 double tmpValue = (Math.pow(-1, (i+j))*tmpDeter);
            	 ligne[j] =  tmpValue;
             }
             a.ajouterLigne(new Ligne(m.getNbcol(),ligne));
         }
		return a;	
	}
	/*
	public static Matrice transformerComatrice(Matrice m)
	{
		int nbLigne = m.getNbligne();
		int nbColonne = m.getNbcol();
		double tmpVal = 0;
		for(int currentRow = 0;currentRow < nbLigne;currentRow++)
		{
			for(int currentCol = 0; currentCol < nbColonne; currentCol++)
			{
				tmpVal = m.getLigne(currentRow).get(currentCol);
				m.getLigne(currentRow).modifierCelulle(currentCol, m.getLigne(currentCol).get(currentRow));
				m.getLigne(currentCol).modifierCelulle(currentRow, tmpVal);
			}
		}
		return m;
	}
	*/
	public static Matrice transformerComatrice(Matrice m)
	{
		Matrice a = new Matrice(m.getNbcol(),m.getNbligne());
		double[] ligne = new double[m.getNbligne()];
		for(int i=0; i<m.getNbcol();i++) {
			for(int j=0;j<m.getNbligne();j++) {
				ligne[j]=m.getLigne(j).get(i);
			}
			a.ajouterLigne(new Ligne(m.getNbligne(),ligne));
		}
		return a;
	}
	public static Matrice inverse(Matrice m){
		Matrice tmpCoffacteur = coffacteur(m);
		double tmpDeter = determinant(m);
		System.out.println("Coffacteur dans inverse");
		System.out.println(tmpCoffacteur.toString());
		System.out.println("Determinant dans inverse");
		System.out.println(tmpDeter);
		System.out.println("1 DIVISER PAR TRUC");
		System.out.println((1./tmpDeter));
		System.out.println("matrice transpos�e"+transformerComatrice(tmpCoffacteur));
		System.out.println("Resultat finale matrice inverse");
		System.out.println(produitScalaire((1./tmpDeter), transformerComatrice(tmpCoffacteur)).toString());
		return produitScalaire((1./tmpDeter), transformerComatrice(tmpCoffacteur));	
	}
	
	public static Matrice creationIdentite(Matrice m){
		Matrice matriceId = new Matrice(m.getNbligne(), m.getNbcol()-1);
		double ligneTmp[] = new double[m.getNbcol()-1];
		for(int i=0; i<m.getNbligne();i++){
			for(int j=0;j<m.getNbcol()-1;j++){
				if(i==j){
					ligneTmp[j] = 1;
				}else{
					ligneTmp[j] = 0;
				}
			}
			matriceId.ajouterLigne(new Ligne(m.getNbcol()-1,ligneTmp));
		}
		return matriceId;
	}
}
